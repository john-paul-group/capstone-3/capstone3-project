import React from "react";
import ServiceCard from "./ServiceCard";
import { Col } from "react-bootstrap";

import weatherImg from "../assets/images/weather.png";
import guideImg from "../assets/images/guide.png";
import customizationImg from "../assets/images/customization.png";

const servicesData = [
  {
    imgUrl: weatherImg,
    title: "Calculate Weather",
    desc: "The application of science and technology to predict the conditions of the atmosphere for a given location and time.",
  },
  {
    imgUrl: guideImg,
    title: "Best Tour Guide",
    desc: "A person who provides assistance, information on cultural, historical and contemporary heritage to people on organized sightseeing and individual clients at educational establishments, religious and historical sites such as; museums, and at various venues of tourist attraction resorts.",
  },
  {
    imgUrl: customizationImg,
    title: "Customization",
    desc: "You can customized your own trip!.",
  },
];

const ServiceList = () => {
  return (
    <>
      {servicesData.map((item, index) => (
        <Col lg="3" md="6" sm="12" className="mb-4" key={index}>
          <ServiceCard item={item} />
        </Col>
      ))}
    </>
  );
};

export default ServiceList;
