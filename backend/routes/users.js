import express from "express";
import { updateUser, deleteUser, getSingleUser, getAllUser} from "../controllers/userController.js";
import { verifyAdmin, verifyUser } from "../utils/verifyToken.js";

const router = express.Router();

// Update user

router.put("/:id", verifyUser, updateUser);

// Delete user

router.delete("/:id", verifyUser, deleteUser);

//Get Single user

router.get("/:id", verifyUser, getSingleUser);

//Get All user

router.get("/", verifyAdmin, getAllUser);

export default router;