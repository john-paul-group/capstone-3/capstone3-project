import express from "express"
import { createTour, updateTour, deleteTour, getSingleTour, getAllTour, getTourBySearch, getFeaturedTours, getTourCount } from "./../controllers/tourController.js"
import {verifyAdmin, verifyUser} from '../utils/verifyToken.js';
const router = express.Router()

// create new Tour

router.post("/", verifyAdmin, createTour);

// Update Tour

router.put("/:id",verifyAdmin, updateTour);

// Delete Tour

router.delete("/:id",verifyAdmin, deleteTour);

//Get Single Tour

router.get("/:id", getSingleTour);

//Get All Tour

router.get("/", getAllTour);

// Get tour be search
router.get("/search/getTourBySearch", getTourBySearch);

// Get tour be featured
router.get("/search/getFeaturedTours", getFeaturedTours);

router.get("/search/getTourCount", getTourCount);


export default router;